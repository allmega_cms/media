<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Model;

use Allmega\BlogBundle\Utils\File;

interface MediatypeInterface
{
    /**
     * @return array<int,File>
     */
    public function getFiles(): array;
    public function getName(): ?string;
    public function getMimetypes(): ?string;
    public function getMimetypesArray(): array;
    public function checkMimetype(string $Mimetype): bool;
}