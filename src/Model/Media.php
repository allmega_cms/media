<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Model;

use Allmega\AuthBundle\Entity\Group;
use Allmega\BlogBundle\Utils\IdGenerator;
use Allmega\BlogBundle\Model\{ItemInfo, SortableItemInterface};
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

#[UniqueEntity('title', message: 'errors.exists')]
abstract class Media extends ItemInfo implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    protected ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    protected ?string $title = null;

    #[ORM\Column(length: 191)]
    protected string $slug = '';

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    protected ?string $description = null;

    protected Collection $groups;
    protected string $url = '';

    public function __construct()
    {
        parent::__construct();
        $this->groups  = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;
        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int,Group>
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }
    public function addGroup(Group $group): static
    {
        if (!$this->groups->contains($group)) $this->groups[] = $group;
        return $this;
    }

    public function removeGroup(Group $group): static
    {
        if ($this->groups->contains($group)) $this->groups->removeElement($group);
        return $this;
    }

    abstract static function getSortableProps(): array;

    public static function getBundleName(): string
    {
        return 'Media';
    }
}