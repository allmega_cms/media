<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Model;

use Allmega\AuthBundle\Entity\Group;
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\BlogBundle\Data as BlogData;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

trait MediaTypeTrait
{
    private function buildBaseForm(FormBuilderInterface &$builder, Gallery $gallery, string $type = 'gallery'): void
    {
        if (!$gallery->isWeb() && $this->security->isGranted('ROLE_MEDIA_MANAGER')) {
            $builder
                ->add('groups', EntityType::class, [
                    'label' => $type . '.label.groups',
                    'help'  => $type . '.help.groups',
                    'help_translation_parameters' => [
                        '%multiplemark%' => $this->translator->trans('action.multiplemark', [], BlogData::DOMAIN)
                    ],
                    'help_html' => true,
                    'multiple' => true,
                    'required' => false,
                    'class' => Group::class,
                    'choice_label'  => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('g')->where('g.selectable = 1')->orderBy('g.name', 'ASC');
                    }
                ]);
        }

        $builder
            ->add('title', null, [
                'label' => $type . '.label.title',
                'attr' => ['placeholder' => $type . '.label.title']
            ])
            ->add('description', null, [
                'label' => 'label.description',
                'attr' => ['placeholder' => 'label.description']
            ]);
    }
}