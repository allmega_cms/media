<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle;

final class Events
{
    public const MEDIAFILE_CREATED = 'mediafile.created';
    public const MEDIAFILE_UPDATED = 'mediafile.updated';
    public const MEDIAFILE_DELETED = 'mediafile.deleted';
	public const DELETE_FILES  = 'mediafile.alldeleted';
    public const GALLERY_CREATED = 'gallery.created';
    public const GALLERY_UPDATED = 'gallery.updated';
    public const GALLERY_UPDATE_FAILED = 'gallery.update_failed';
    public const GALLERY_UPDATE_FAILED_DIFF = 'gallery.update_failed_diff';
    public const GALLERY_DELETE_FAILED_SYSTEM = 'gallery.delete_failed.system';
    public const GALLERY_DELETE_FAILED_NOT_EMPTY = 'gallery.delete_failed.not_empty';
    public const GALLERY_DELETED = 'gallery.deleted';
    public const FILELOAD_CREATED = 'mediafile.created';
    public const MEDIATYPE_CREATED = 'mediatype.created';
    public const MEDIATYPE_UPDATED = 'mediatype.updated';
    public const MEDIATYPE_DELETED = 'mediatype.deleted';
    public const MEDIATYPE_STATE_CHANGED = 'mediatype.state_changed';
}