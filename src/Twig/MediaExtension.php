<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Twig;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Utils\Twig\FilesWidget;
use Allmega\MediaBundle\Manager\MediaFileHandler;
use Allmega\MediaBundle\Controller\FileLoadController;
use Twig\Extension\AbstractExtension;
use Twig\{TwigFunction, Environment};

class MediaExtension extends AbstractExtension
{
    public const TEMPLATE_PATH = '@AllmegaMedia/twig/';

    public function __construct(private readonly MediaFileHandler $mediaFileHandler) {}

    public function getFunctions(): array
    {
        return [
            new TwigFunction('allmega_media_render_files_widget', [$this, 'filesWidget'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function filesWidget(Environment $env, array $params): string
    {
        $params['mediatypes'] = $this->mediaFileHandler->setDir($params['folder'])->getMediatypes();
        $params['viewParams'] = [
            'route' => FileLoadController::ROUTE_NAME,
            'path' => self::TEMPLATE_PATH . 'files/',
            'domain' => Data::DOMAIN,
        ];
        return (new FilesWidget($env, $params))->view();
    }
}