<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Repository;

use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class MediaFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MediaFile::class);
    }

    public function findAllQuery(Gallery $gallery): Query
    {
        return $this->createQueryBuilder('a')
            ->join('a.gallery', 'g')
            ->where('g.id = :id')
            ->orderBy('a.title', 'ASC')
            ->setParameter('id', $gallery->getId())
            ->getQuery();
    }
}