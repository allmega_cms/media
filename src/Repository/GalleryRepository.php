<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Entity\Gallery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GalleryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gallery::class);
    }

    public function findChilds(Gallery $gallery): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.parent = :id')
            ->setParameter('id', $gallery->getId())
            ->getQuery()
            ->getResult();
    }

    public function setToPrevParent(Gallery $gallery): void
    {
        $parentGallery = $gallery->getParent();
        $id = $parentGallery ? $parentGallery->getId() : null;

        $this->createQueryBuilder('g')
            ->update()
            ->set('g.parent', ':id')
            ->where('g.parent = :pid')
            ->setParameter('id', $id)
            ->setParameter('pid', $gallery->getId())
            ->getQuery()
            ->execute();
    }

    public function findByUserGroups(User $user): array
    {
        $ids = [];
        foreach ($user->getGroups() as $group) $ids[] = $group->getId();
        return $this->createQueryBuilder('m')
            ->join('m.groups', 'g')
            ->where('g.id IN (:ids)')
            ->orderBy('m.title', 'ASC')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function findWithoutGroups(): array
    {
        $ids = $this->findIdsFromGalleriesWithGroups();
        $query = $this->createQueryBuilder('g');
        if ($ids) $query->where('g.id NOT IN (:ids)')->setParameter('ids', $ids);
        return $query->orderBy('g.title', 'ASC')->getQuery()->getResult();
    }

    protected function findIdsFromGalleriesWithGroups(): array
    {
        $sql = 'SELECT DISTINCT g.gallery_id FROM allmega_media__gallery_group g';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $rows = $stmt->executeQuery()->fetchAllAssociative();

        $ids = [];
        foreach ($rows as $row) $ids[] = $row['gallery_id'];
        return $ids;
    }
}