<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\EventSubscriber;

use Allmega\MediaBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MEDIAFILE_CREATED => 'addSuccessFlash',
            Events::MEDIAFILE_UPDATED => 'addSuccessFlash',
            Events::MEDIAFILE_DELETED => 'addSuccessFlash',
            Events::GALLERY_CREATED => 'addSuccessFlash',
            Events::GALLERY_UPDATED => 'addSuccessFlash',
            Events::GALLERY_UPDATE_FAILED => 'addWarningFlash',
            Events::GALLERY_UPDATE_FAILED_DIFF => 'addWarningFlash',
            events::GALLERY_DELETE_FAILED_SYSTEM => 'addWarningFlash',
            Events::GALLERY_DELETE_FAILED_NOT_EMPTY => 'addWarningFlash',
            Events::GALLERY_DELETED => 'addSuccessFlash',
        ];
    }
}