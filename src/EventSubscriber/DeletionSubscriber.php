<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\EventSubscriber;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\FlashBag;
use Allmega\MediaBundle\{Data, Events};
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\MediaBundle\Manager\MediaFileHandler;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

readonly class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MediaFileHandler $mediafileHandler,
        private EntityManagerInterface $em,
        private FlashBag $flashbag) {}

    public static function getSubscribedEvents(): array
    {
        return [
            AuthEvents::USER_DELETE => 'handleUserRelations',
            Events::DELETE_FILES => 'deleteFiles',
        ];
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $event->getSubject();

        $items = [Gallery::class, MediaFile::class];

        foreach ($items as $item) {
            $userRepo
                ->updateField($item, 'creator', $user)
                ->updateField($item, 'editor', $user);
        }
    }

    public function deleteFiles(GenericEvent $event, string $eventName): void
    {
        if ($event->hasArgument('folder')) {
            $dir = $event->getArgument('folder');
            $this->mediafileHandler->setDir($dir)->remove();
    
            $isXmlRequest = $event->getArgument('isXmlRequest');
            if (!$isXmlRequest) $this->flashbag->add('success', $eventName, Data::DOMAIN);

        }
    }
}