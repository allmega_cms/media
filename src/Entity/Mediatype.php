<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\MediaBundle\Entity;

use Allmega\MediaBundle\Model\MediatypeInterface;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\MediaBundle\Repository\MediatypeRepository;
use Allmega\BlogBundle\Utils\{File, Helper, IdGenerator};
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity(fields: 'shortname', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: MediatypeRepository::class)]
#[ORM\Table(name: '`allmega_media__mediatype`')]
class Mediatype implements MediatypeInterface, SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $name = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $shortname = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT, length: 500, nullable: true)]
    #[Assert\Length(max: 500, maxMessage: 'errors.max_value')]
    private ?string $mimetypes = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $sys = false;
    private array $files = [];

    /**
     * Create a new Mediatype entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $name, $description, $mimetypes, $shortname as dummy text
     */
    public static function build(
        string $shortname = null,
        string $mimetypes = null,
        bool $active = false,
        bool $sys = false,
        string $name = null,
        string $description = null): static
    {
        $description = $description ?? Helper::generateRandomString();
        $shortname = $shortname ?? Helper::generateRandomString();
        $mimetypes = $mimetypes ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setMimetypes($mimetypes)
            ->setShortname($shortname)
            ->setActive($active)
            ->setName($name)
            ->setSys($sys);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getMimetypes(): ?string
    {
        return $this->mimetypes;
    }

    public function setMimetypes(?string $mimetypes): static
    {
        $this->mimetypes = $mimetypes;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function isSys(): bool
    {
        return $this->sys;
    }

    public function setSys(bool $sys): static
    {
        $this->sys = $sys;
        return $this;
    }

	public function getFiles(): array
    {
		return $this->files;
	}

	public function setFiles(array $files = []): static
    {
		$this->files = $files;
        return $this;
	}

	public function addFile(File $file): static
    {
		$this->files[] = $file;
		return $this;
	}

    public function checkMimetype(string $mimetype): bool
    {
        return in_array($mimetype, $this->getmimetypesArray());
    }

    public function getMimetypesArray(): array
    {
        return $this->mimetypes ? preg_split('/[\s,]+/', $this->mimetypes) : [];
    }

    public static function getSortableProps(): array
    {
        return ['name', 'active'];
    }

    public static function getBundleName(): string
    {
        return 'Media';
    }
}