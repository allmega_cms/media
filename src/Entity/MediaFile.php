<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\MediaBundle\Model\Media;
use Allmega\AuthBundle\Entity\{Group, User};
use Allmega\MediaBundle\Repository\MediaFileRepository;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: MediaFileRepository::class)]
#[ORM\Table(name: '`allmega_media__file`')]
class MediaFile extends Media
{
    #[ORM\Column(length: 191)]
    protected string $filename = '';

    #[ORM\Column(length: 191)]
    protected string $filetype = '';

    #[ORM\Column(type: Types::INTEGER)]
    protected int $filesize = 0;

    #[ORM\Column(type: Types::INTEGER)]
    protected int $hits = 0;

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: '`allmega_media__file_group`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    protected Collection $groups;

    #[ORM\ManyToOne(targetEntity: Gallery::class, inversedBy: 'mediafiles')]
    protected ?Gallery $gallery;

    /**
     * Create a new MediaFile entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $title, $description as dummy text
     * - $user, $gallery will be created
     */
    public static function build(
        string $description = null,
        Gallery $gallery = null,
        User $user = null,
        string $title = null): static
    {
        $user = $user ?? User::build();
        $gallery = $gallery ?? Gallery::build(user: $user);
        $description = $description ?? Helper::generateRandomString();
        $title = $title ?? Helper::generateRandomString();
        $slug = (new AsciiSlugger())->slug($title);

        return (new static())
            ->setDescription($description)
            ->setGallery($gallery)
            ->setCreator($user)
            ->setEditor($user)
            ->setTitle($title)
            ->setSlug($slug);
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): static
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFiletype(): string
    {
        return $this->filetype;
    }

    public function setFiletype(string $filetype): static
    {
        $this->filetype = $filetype;
        return $this;
    }

    public function getFilesize(): int
    {
        return $this->filesize;
    }
    public function setFilesize(int $filesize): static
    {
        $this->filesize = $filesize;
        return $this;
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function setHits(int $hits): static
    {
        $this->hits = $hits;
        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): static
    {
        $this->gallery = $gallery;
        return $this;
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = $this->gallery->getMediaDir();
    }

    public function getRelativePath(): string
    {
        $filename = $this->isWeb() ? $this->filename : $this->id;
        return $this->getMediaDir() . DIRECTORY_SEPARATOR . $filename;
    }

    public function isWeb(): bool
    {
        return $this->gallery->isWeb();
    }

    public static function getSortableProps(): array
    {
        return ['title', 'filename', 'filetype', 'filesize', 'hits', 'created'];
    }

    public function getBase64Image(string $path): string
    {
        return Helper::getBase64Content($path . $this->getRelativePath());
    }
}