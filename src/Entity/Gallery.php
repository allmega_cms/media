<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\MediaBundle\Model\Media;
use Allmega\AuthBundle\Entity\{Group, User};
use Allmega\MediaBundle\Repository\GalleryRepository;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: GalleryRepository::class)]
#[ORM\Table(name: '`allmega_media__gallery`')]
class Gallery extends Media
{
    #[ORM\ManyToOne(targetEntity: Gallery::class)]
    protected ?Gallery $parent = null;

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: '`allmega_media__gallery_group`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    protected Collection $groups;

    #[ORM\OneToMany(targetEntity: MediaFile::class, mappedBy: 'gallery')]
    #[ORM\OrderBy(['title' => 'ASC'])]
    protected Collection $mediafiles;

    #[ORM\Column(type: Types::BOOLEAN)]
    protected bool $sys = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    protected bool $web = false;
    protected array $parents = [];

    /**
     * Create a new Gallery entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $title, $description as dummy text
     * - $slug will be generated from $title
     * - $author will be created
     */
    public static function build(
        string $title = null,
        string $slug = null,
        bool $web = false,
        bool $sys = false,
        array $groups = [],
        User $user = null,
        Gallery $parent = null,
        string $description = null): static
    {
        $title = $title ?? Helper::generateRandomString();
        $description = $description ?? Helper::generateRandomString();
        $slug = $slug ?? (new AsciiSlugger())->slug($title);
        $user = $user ?? User::build();

        $gallery = (new static())
            ->setDescription($description)
            ->setParent($parent)
            ->setCreator($user)
            ->setEditor($user)
            ->setTitle($title)
            ->setSlug($slug)
            ->setWeb($web)
            ->setSys($sys);

        foreach ($groups as $group) $gallery->addGroup($group);
        return $gallery;
    }

    public function __construct()
    {
        parent::__construct();
        $this->mediafiles = new ArrayCollection();
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?Gallery $parent): static
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Collection<int,MediaFile>
     */
    public function getMediafiles(): Collection
    {
        return $this->mediafiles;
    }

    public function addMediafile(MediaFile $mediafile): static
    {
        if (!$this->mediafiles->contains($mediafile)) {
            $this->mediafiles[] = $mediafile;
            $mediafile->setGallery($this);
        }

        return $this;
    }

    public function removeMediafile(MediaFile $mediafile): static
    {
        if ($this->mediafiles->removeElement($mediafile)) {
            if ($mediafile->getGallery() === $this) $mediafile->setGallery(null);
        }

        return $this;
    }

    public function isSys(): bool
    {
        return $this->sys;
    }

    public function setSys(bool $sys): static
    {
        $this->sys = $sys;
        return $this;
    }

    public function isWeb(): bool
    {
        return $this->web;
    }

    public function setWeb(bool $web): static
    {
        $this->web = $web;
        return $this;
    }

    public function getParents(): array
    {
        $this->setParents($this);
        return array_reverse($this->parents);
    }

    private function setParents(Gallery $gallery): void
    {
        $this->parents[] = $gallery;
        $parent = $gallery->getParent();
        if ($parent) $this->setParents($parent);
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = $this->slug;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'created'];
    }
}