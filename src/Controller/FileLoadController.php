<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Controller;

use Allmega\MediaBundle\Data;
use Allmega\BlogBundle\Utils\File;
use Allmega\MediaBundle\Manager\FileLoadControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Upload, download, manage and delete files from another packages
 */
#[Route('/fileload', name: 'allmega_media_fileload_')]
class FileLoadController extends BaseController
{
    use FileLoadControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMedia/fileload/';
    public const ROUTE_NAME = 'allmega_media_fileload_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/gallery/{folder}', name: 'gallery', methods: 'GET')]
    #[IsGranted('media-fileload-show')]
    public function gallery(string $folder): Response
    {
        $mediatypes = $this->getMediaFileHandler()->setDir($folder)->getMediatypes('pictures');
        $data['content'] = $this->renderView(self::ROUTE_TEMPLATE_PATH . 'carousel.html.twig', [
            'mediatype' => $mediatypes[0]
        ]);
        return $this->json($data);
    }

    #[Route('/add/{folder}', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('media-fileload-add')]
    public function add(string $folder): Response
    {
        return $this->save($folder);
    }

    #[Route('/load/{filename}/{folder}/{route}/{type}/{id}', name: 'load', requirements: ['type' => 'blob|base64'], methods: 'GET')]
    #[IsGranted('media-fileload-load')]
    public function load(string $filename, string $folder, string $route, string $type = 'blob', string $id = ''): Response
    {
        $params = $id ? ['id' => $id] : [];

        $filename = $this->getMediaFileHandler()->setDir($folder)->getDir() . DIRECTORY_SEPARATOR . $filename;
        $filesystem = new Filesystem();
        
        if ($filesystem->exists($filename)) {
            if ($type == 'blob') return $this->file($filename);
            elseif ($type == 'base64') {
                $file = new SplFileInfo($filename, $this->getMediaFileHandler()->getDir(), 'file_load');
                if ($file) {
                    $data['content'] = (new File($file))->getBase64EncodedContent();
                    return $this->json($data);
                }
            }
        }
        return $this->redirectToRoute($route, $params);
    }

    #[Route('/delete/{filename}/{folder}/{route}/{prop}/{id}', name: 'delete', methods: 'GET')]
    #[IsGranted('media-fileload-delete')]
    public function delete(string $filename, string $folder, string $route, string $prop, string $id = ''): Response
    {
        $params = $id ? [$prop => $id] : [];
        $this->getMediaFileHandler()->setDir($folder)->remove($filename);
        $this->getFlashBag()->add('success', 'mediafile.deleted', Data::DOMAIN);
        return $this->redirectToRoute($route, $params);
    }
}