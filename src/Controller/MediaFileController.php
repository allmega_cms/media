<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Controller;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Events;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\MediaBundle\Manager\{MediaFileControllerTrait, MediaFileHandler};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/mediafile', name: 'allmega_media_mediafile_')]
class MediaFileController extends BaseController
{
    use MediaFileControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMedia/mediafile/';
    public const ROUTE_NAME = 'allmega_media_mediafile_';

    public function __construct(
        private readonly MediaFileHandler $mediafileHandler,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/load/{id}', name: 'load', methods: 'GET')]
    #[IsGranted('media-file-load', subject: 'mediafile')]
    public function load(MediaFile $mediafile): Response
    {
        $hits = $mediafile->getHits() + 1;
        $mediafile->setHits($hits);
        $this->getEntityManager()->flush();

        $path = $this->mediafileHandler->getMediaFilePath($mediafile);
        return $this->file($path, $mediafile->getFilename());
    }

    #[Route('/add/{slug}', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('media-file-add', subject: 'gallery')]
    public function add(Gallery $gallery): Response
    {
        return $this->save(null, $gallery);
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('media-file-edit', subject: 'mediafile')]
    public function edit(MediaFile $mediafile): Response
    {
        $clonedMediafile = clone $mediafile;
        $this->mediafileHandler->setClonedMediaFile($clonedMediafile);
        return $this->save($mediafile);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('media-file-delete', subject: 'mediafile')]
    public function delete(MediaFile $mediafile): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $mediafile,
            domain: Data::DOMAIN,
            eventName: Events::MEDIAFILE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}