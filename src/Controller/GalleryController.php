<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Controller;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Utils\{Helper, Paginator, SortableItem};
use Allmega\MediaBundle\Security\Voters\{GalleryVoter, MediaFileVoter};
use Allmega\MediaBundle\Manager\{GalleryControllerTrait, GalleryHandler};
use Allmega\MediaBundle\Repository\{GalleryRepository, MediaFileRepository};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/gallery', name: 'allmega_media_gallery_')]
class GalleryController extends BaseController
{
    use GalleryControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMedia/gallery/';
    public const ROUTE_NAME = 'allmega_media_gallery_';

    public function __construct(
        private readonly GalleryRepository $galleryRepo,
        private readonly GalleryHandler $galleryHandler,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/show/{slug}', name: 'show', defaults: ['slug' => Data::DOWNLOADS_GALLERY], methods: 'GET')]
    #[IsGranted('media-gallery-show', subject: 'gallery')]
    public function show(
        MediaFileRepository $mediaRepo,
        MediaFileVoter $mediafileVoter,
        GalleryVoter $galleryVoter,
        Paginator $paginator,
        Gallery $gallery): Response
    {
        $childGalleries = $this->galleryRepo->findChilds($gallery);
        $galleriesPath = $gallery->isWeb() ?
            $this->galleryHandler->getPublicGalleriesDir() : 
            $this->galleryHandler->getGalleriesDir();

        $query = $mediaRepo->findAllQuery($gallery);

        $optParams = [
            'childGalleries' => $childGalleries,
            'mediafileVoter' => $mediafileVoter,
            'galleryVoter' => $galleryVoter,
            'media' => [
                'path' => MediaFileController::ROUTE_TEMPLATE_PATH,
                'route' => MediaFileController::ROUTE_NAME,
            ]
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'item' => SortableItem::getInstance(new MediaFile()),
            'mediafiles' => $paginator->getPagination($query),
            'galleriesPath' => $galleriesPath,
            'gallery' => $gallery
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('media-gallery-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('media-gallery-edit', subject: 'gallery')]
    public function edit(Gallery $gallery): Response
    {
        $clonedGallery = clone $gallery;
        $this->galleryHandler->setClonedGallery($clonedGallery);
        return $this->save($gallery);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('media-gallery-delete', subject: 'gallery')]
    public function delete(Gallery $gallery): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $gallery,
            domain: Data::DOMAIN,
            eventName: Events::GALLERY_DELETED,
            routeName: self::ROUTE_NAME,
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/menu', name: 'menu', methods: 'GET')]
    #[IsGranted('media-gallery-menu')]
    public function buildMenu(GalleryVoter $galleryVoter): Response
    {
        $optParams = ['galleryVoter' => $galleryVoter];
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'inc/_menu.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'galleries' => Helper::sort($this->findMenuGalleries()),
        ]);
    }

    #[Route('/icon', name: 'icon', methods: 'GET')]
    public function getMenuIcon(): Response
    {
        if (!$this->isGranted('media-gallery-icon')) return new Response();

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'inc/_icon.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }
}