<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\MediaBundle\Controller;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Entity\Mediatype;
use Allmega\MediaBundle\Repository\MediatypeRepository;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\MediaBundle\Manager\MediatypeControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/mediatype', name: 'allmega_media_mediatype_')]
class MediatypeController extends BaseController
{
    use MediatypeControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMedia/mediatype/';
    public const ROUTE_NAME = 'allmega_media_mediatype_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('media-type-list')]
    public function index(MediatypeRepository $mediatypeRepo, Paginator $paginator): Response
    {
        $query = $mediatypeRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Mediatype()),
            'mediatypes' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('media-type-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('media-type-edit', subject: 'mediatype')]
    public function edit(Mediatype $mediatype): Response
    {
        return $this->save($mediatype);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('media-type-delete', subject: 'mediatype')]
    public function delete(Mediatype $mediatype): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $mediatype,
            domain: Data::DOMAIN,
            eventName: Events::MEDIATYPE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('media-type-state', subject: 'mediatype')]
    public function changeState(Mediatype $mediatype): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $mediatype,
            domain: Data::DOMAIN,
            eventName: Events::MEDIATYPE_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}