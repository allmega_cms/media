<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Security\Voters;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FileLoadVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $load = 'load';

    protected function supports(string $attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'media-fileload', [$this->load]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->load:
            case $this->show:
                $result = true;
                break;
            case $this->add:
            case $this->delete:
                $result = $this->hasRole($user);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return false;
    }
}