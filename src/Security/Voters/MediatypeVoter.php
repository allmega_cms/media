<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Security\Voters;

use Allmega\MediaBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Entity\Mediatype;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MediatypeVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'media-type');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;
        
        switch ($attribute) {
            case $this->delete:
            case $this->state:
            case $this->list:
            case $this->edit:
            case $this->add:
                $result = $this->hasRole($user, Data::MANAGER_ROLE);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Mediatype;
    }
}