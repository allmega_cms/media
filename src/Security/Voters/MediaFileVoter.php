<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Security\Voters;

use Allmega\MediaBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MediaFileVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $info = 'info';
    protected string $load = 'load';

    protected function supports(string $attribute, mixed $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'media-file', [$this->info, $this->load]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        $isManager = $this->hasRole($user, Data::MANAGER_ROLE);
        $isUser = $this->hasRole($user, Data::USER_ROLE);

        $hasAccess = $isManager || $isUser && $this->hasAccessOnSubject($subject, $user);
        $hasGroups = $this->countSubjectGroups($subject) > 0;

        switch ($attribute) {
            case $this->load:
                $result = !$hasGroups || $hasAccess;
                break;
            case $this->delete:
            case $this->edit:
            case $this->info:
            case $this->add:
                $result = $hasAccess;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof MediaFile || $subject instanceof Gallery;
    }
}