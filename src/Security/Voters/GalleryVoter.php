<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Security\Voters;

use Allmega\MediaBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class GalleryVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $menu = 'menu';
    protected string $icon = 'icon';
    protected string $info = 'info';
    protected string $web = 'web';

    protected function supports(string $attribute, mixed $subject): bool
    {
        $extraAttributes = [$this->icon, $this->info, $this->menu, $this->web];
        $voterParams = $this->createVoterParams($attribute, $subject, 'media-gallery', $extraAttributes);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        $isManager = $this->hasRole($user, Data::MANAGER_ROLE);
        $isUser = $this->hasRole($user, Data::USER_ROLE);
        $isAuthor = $this->isBlogAuthor($user);

        $hasAccess = $isManager || $isUser && $this->hasAccessOnSubject($subject, $user);
        $hasGroups = $this->countSubjectGroups($subject) > 0;

        switch ($attribute) {
            case $this->menu:
                $result = true;
                break;
            case $this->show:
                $result = !$hasGroups || $hasAccess;
                break;
            case $this->info:
            case $this->add:
            case $this->icon:
                $result = $isUser;
                break;
            case $this->web:
                $result = $isManager || $isAuthor;
                break;
            case $this->delete:
            case $this->edit:
                $result = $hasAccess;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Gallery;
    }
}