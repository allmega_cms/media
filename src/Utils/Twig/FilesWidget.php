<?php

/**
 * @package   Allmega
 * @copyright Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Utils\Twig;

use Twig\Environment;

class FilesWidget
{
    private array $viewParams;
    private array $mediatypes;
    private string $folder;
    private string $route;
    private string $navid;
    private string $prop;
    private string $info;
    private string $id;
    private bool $show;
    private int $count;

    public function __construct(private Environment $env, array $params)
    {
        $folder = $route = $id = $navid = '';
        $viewParams = $mediatypes = [];
        $show = false;
        $prop = 'id';

        extract($params);

        $this->viewParams = $viewParams;
        $this->mediatypes = $mediatypes;
        $this->folder = $folder;
        $this->route = $route;
        $this->navid = $navid;
        $this->prop = $prop;
        $this->show = $show;
        $this->id = $id;
        
        $this->setInfo()->setCount();
    }

    public function view(): string
    {
        return $this->env->render('@AllmegaMedia/twig/files/_slider.html.twig', ['filesWidget' => $this]);
    }

    public function getViewParams(): array
    {
		return $this->viewParams;
	}

	public function setViewParams(array $viewParams): static
    {
		$this->viewParams = $viewParams;
		return $this;
	}

	public function getMediatypes(): array
    {
		return $this->mediatypes;
	}

	public function setMediatypes(array $mediatypes): static
    {
		$this->mediatypes = $mediatypes;
		return $this;
	}

    public function getShow(): bool
    {
        return $this->show;
    }

    public function setShow(bool $show): static
    {
        $this->show = $show;
        return $this;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function setRoute(string $route): static
    {
        $this->route = $route;
        return $this;
    }

    public function getFolder(): string
    {
        return $this->folder;
    }

    public function setFolder(string $folder): static
    {
        $this->folder = $folder;
        return $this;
    }

	public function getNavid(): string
    {
		return $this->navid;
	}

	public function setNavid(string $navid): static
    {
		$this->navid = $navid;
		return $this;
	}

    public function getProp(): string
    {
        return $this->prop;
    }

    public function setProp(string $prop): static
    {
        $this->prop = $prop;
        return $this;
    }

	public function getInfo(): string
    {
		return $this->info;
	}

	public function setInfo(): static
    {
		$this->info = $this->show ? 'mediafile.info.manage' : 'mediafile.info.list';
		return $this;
	}

	public function getId(): string
    {
		return $this->id;
	}

	public function setId(string $id): static
    {
		$this->id = $id;
		return $this;
	}

	public function getCount(): int
    {
		return $this->count;
	}

	private function setCount(): static
    {
        $count = 0;
        foreach ($this->mediatypes as $mediatype) {
            $count += count($mediatype->getFiles());
        }
		$this->count = $count;
		return $this;
	}
}