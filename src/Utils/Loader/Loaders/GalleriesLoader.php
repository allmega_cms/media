<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Repository\GroupRepository;
use Allmega\AuthBundle\Entity\{User, Group};
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\AuthBundle\Data as AuthData;

class GalleriesLoader extends AbstractLoader
{
    protected GroupRepository $groupRepo;
    protected User $author;

    public function init(): static
    {
        $this->transId = 'loader.galleries.message';
        $this->title = 'loader.galleries.phrase';
        $this->class = Gallery::class;
        $this->prop = 'slug';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        $prop = strtolower($item->getTitle());
        $description = $this->trans("galleries.$prop.description", $this->currentPackage);
        $title = $this->trans("galleries.$prop.title", $this->currentPackage);

        $item->setTitle($title)->setDescription($description);
        $this->addGroups($item);

        if ($existsItem) {
            $existsItem
                ->setDescription($item->getDescription())
                ->setTitle($item->getTitle())
                ->setSlug($item->getSlug())
                ->setWeb($item->isWeb())
                ->setSys($item->isSys());

            foreach ($item->getGroups() as $group) {
                if (!$existsItem->getGroups()->contains($group)) {
                    $existsItem->addGroup($group);
                }
            }
            $item = $existsItem;
        } else {
            $user = $this->em->getRepository(User::class)->findOneByRole(AuthData::USER_ROLE);
            $item->setCreator($user)->setEditor($user);
        }
    }

    protected function addGroups(Gallery &$item): void
    {
        if ($item->getGroups()->count()) {
            $shortnames = [];
            foreach ($item->getGroups() as $group) $shortnames[] = $group->getShortname();
            $groups = $this->em->getRepository(Group::class)->findByShortNames($shortnames);
            $item->getGroups()->clear();
            foreach ($groups as $group) $item->addGroup($group);
        }
    }
}