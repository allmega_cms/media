<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\MediaBundle\Entity\MediaType;

class MediaTypesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.mediatypes.message';
        $this->title = 'loader.mediatypes.phrase';
        $this->class = MediaType::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        $description = $this->trans("storage.mediatypes.{$item->getShortname()}.description", $this->currentPackage);
        $name = $this->trans("storage.mediatypes.{$item->getShortname()}.name", $this->currentPackage);
        
        $item->setName($name)->setDescription($description);

        if ($existsItem) {
            $item = $existsItem
                ->setDescription($item->getDescription())
                ->setMimetypes($item->getMimetypes())
                ->setShortname($item->getShortname())
                ->setName($item->getName())
                ->setSys($item->isSys());
        }
    }
}