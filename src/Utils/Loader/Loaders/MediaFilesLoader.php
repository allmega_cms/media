<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\AuthBundle\Entity\User;

class MediaFilesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.mediafiles.message';
        $this->title = 'loader.mediafiles.phrase';
        $this->class = MediaFile::class;
        $this->prop = 'title';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        $gallery = $this->em
            ->getRepository(Gallery::class)
            ->findOneBy(['title' => $item->getGallery()->getTitle()]);
        
        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy(['username' => $item->getCreator()->getUsername()]);

        $item->setGallery($gallery)->setCreator($user)->setEditor($user);
    }
}