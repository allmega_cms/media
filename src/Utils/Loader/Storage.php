<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Utils\Loader;

use Allmega\MediaBundle\Utils\Loader\Loaders\{GalleriesLoader, MediaFilesLoader, MediaTypesLoader};
use Allmega\BlogBundle\Utils\Loader\Model\AbstractStorage;

class Storage extends AbstractStorage
{
    protected array $props = ['galleries', 'mediafiles', 'mediatypes'];
    protected array $mediafiles = [];
    protected array $mediatypes = [];
    protected array $galleries = [];

    public function getLoaders(): array
    {
        return [
            MediaFilesLoader::class => $this->mediafiles,
            MediaTypesLoader::class => $this->mediatypes,
            GalleriesLoader::class => $this->galleries,
        ];
    }
}