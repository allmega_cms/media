<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Form\MediaFileType;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\MediaBundle\Controller\GalleryController;
use Allmega\MediaBundle\Controller\MediaFileController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait MediaFileControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->handleMediafile();
                break;
            case $this->delete:
                $this->mediafileHandler
                    ->setDelete()
                    ->setMediaFile($this->params->getEntity())
                    ->setDir()
                    ->handle();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        $slug = $this->params->getEntity()->getGallery()->getSlug();
        $this->params
            ->setRouteName(GalleryController::ROUTE_NAME, $this->show)
            ->addRouteParams(['slug' => $slug]);
        return $this;
    }

    private function handleMediafile(): void
    {
        $mediafile = $this->params->getEntity();
        if ($this->params->getAction() == $this->add) {
            $mediafile->setCreator($this->getUser());
        }

        $mediafile->setEditor($this->getUser())->setUpdated();

        $file = $this->form['file']?->getData();
        if ($file) $this->mediafileHandler->setUploadedFile($file);

        $this->mediafileHandler->setMediaFile($mediafile)->setDir()->handle();
        $this->params->addFormOptions(['slug' => $mediafile->getGallery()->getSlug()]);
    }

    private function save(MediaFile $mediafile = null, Gallery $gallery = null, array $arguments = []): Response
    {
        $params = $gallery ? ['slug' => $gallery->getSlug()] : [];
        $formParams = $this->buildFormParams($mediafile, $params);
        $gallery = $gallery ?? $mediafile->getGallery();

        $eventName = $mediafile ? Events::MEDIAFILE_UPDATED : Events::MEDIAFILE_CREATED;
        $mediafile = $mediafile ?: (new MediaFile())->setGallery($gallery);

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $mediafile,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: MediaFileType::class,
            routeName: MediaFileController::ROUTE_NAME,
            templatesPath: MediaFileController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params); 
    }

    private function buildFormParams(?MediaFile $mediafile, array $params): array
    {
        return [
            'link' => [
                'route' => GalleryController::ROUTE_NAME . 'show',
                'title' => 'gallery.list',
                'params' => $params,
            ],
            'formParams' => $params,
            'item' => $mediafile
        ];
    }
}