<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Form\MediasType;
use Allmega\MediaBundle\Entity\Mediatype;
use Allmega\MediaBundle\Controller\MediatypeController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait MediatypeControllerTrait
{
    private function save(Mediatype $mediatype = null, array $arguments = []): Response
    {
        $eventName = $mediatype ? Events::MEDIATYPE_UPDATED : Events::MEDIATYPE_CREATED;
        $mediatype = $mediatype ?: new Mediatype();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $mediatype],
            entity: $mediatype,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: MediasType::class,
            routeName: MediatypeController::ROUTE_NAME,
            templatesPath: MediatypeController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}