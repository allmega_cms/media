<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\MediaBundle\Controller\FileLoadController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait FileLoadControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                $this->saveMediafile();
                break;
            default:
        }
        return $this;
    }

    private function saveMediafile(): void
    {
        $files = $this->form['file']->getData();
        if ($files) $this->saveFiles($this->params->getOption('folder'), $files);
        else {
            $this->params->addErrorMessage('mediafile.select_file');
            $this->cancel();
        }
    }

    private function save(string $folder): Response
    {
        $params = (new BaseControllerParams())->init(
            formOptions: ['attr' => ['multiple' => true]],
            options: ['folder' => $folder],
            domain: Data::DOMAIN,
            eventName: Events::FILELOAD_CREATED,
            formPath: FileLoadController::ROUTE_TEMPLATE_PATH,
            formType: FileLoadType::class,
            prop: 'mediafile',
            routeName: FileLoadController::ROUTE_NAME,
            templatesPath: FileLoadController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}