<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\MediaBundle\{Data, Events};
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\MediaBundle\Form\GalleryType;
use Allmega\MediaBundle\Controller\GalleryController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait GalleryControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                $this->addGallery();
                break;
            case $this->edit:
                $this->editGallery();
                break;
            case $this->delete:
                $this->deleteGallery();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        $routeShort = $this->params->getRouteShort();
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $slug = $this->params->getEntity()->getSlug();
                $this->params
                    ->addRouteParams(['slug' => $slug])
                    ->setRouteName($routeShort, $this->show);
                break;
            case $this->delete:
                $this->params->setRouteName($routeShort, $this->show);
                break;
            default:
        }
        return $this;
    }

    private function addGallery(): void
    {
        $gallery = $this->params
            ->getEntity()
            ->setCreator($this->getUser())
            ->setEditor($this->getUser());

        $this->galleryHandler->setGallery($gallery)->handle();
    }

    private function editGallery(): void
    {
        $gallery = $this->params->getEntity();
        if ($gallery->isSys()) {
            $this->params
                ->setEventName(Events::GALLERY_UPDATE_FAILED)
                ->addErrorMessage('gallery.update_failed');
            $this->cancel();
        } elseif ($this->isNotValidParentForWebGallery() || $this->isNotValidParentForMediaGallery()) {
            $this->params
                ->setEventName(Events::GALLERY_UPDATE_FAILED_DIFF)
                ->addErrorMessage('gallery.update_failed_diff');
            $this->cancel();
        } else {
            $gallery->setEditor($this->getUser())->setUdated();
            $this->galleryHandler
                ->setGallery($gallery)
                ->handle();
        }
    }

    private function deleteGallery(): void
    {
        $gallery = $this->params->getEntity();
        if ($gallery->isSys()) {
            $this->params->setEventName(Events::GALLERY_DELETE_FAILED_SYSTEM);
            $this->cancel();
        } elseif ($gallery->getMediafiles()->count()) {
            $this->params->setEventName(Events::GALLERY_DELETE_FAILED_NOT_EMPTY);
            $this->cancel();
        } else {
            $this->galleryHandler
                ->setGallery($gallery)
                ->setDelete()
                ->handle();
        }
    }

    private function isNotValidParentForWebGallery(): bool
    {
        extract($this->getGalleriesForTest());
        /**
         * Web galleries must not be able to be moved into the media galleries and vice versa
         * 
         * Web galleries must not be moved outside of the 'Web' gallery, that's why !$parent
         * and are not allowed to have a other gallery as 'Web' as 'parent'
         */
        return $cloned->isWeb() && !$parent || $cloned->isWeb() && !$parent->isWeb();
    }

    private function isNotValidParentForMediaGallery(): bool
    {
        extract($this->getGalleriesForTest());
        /**
         * Media galleries must not be able to be moved to the web galleries anywhere, even as a root gallery
         */
        return !$cloned->isWeb() && $parent && $parent->isWeb();
    }

    private function getGalleriesForTest(): array
    {
        return [
            'cloned' => $this->galleryHandler->getClonedGallery(),
            'parent' => $this->params->getEntity()->getParent(),
        ];
    }

    private function findMenuGalleries(): array
    {
        $user = $this->getUser();

        if ($this->isGranted('ROLE_MEDIA_MANAGER')) {
            $galleries = $this->galleryRepo->findBy([], ['title' => 'ASC']);
        } else {
            $galleries = $this->galleryRepo->findWithoutGroups();
            if ($user) {
                $rows = $this->galleryRepo->findByUserGroups($user);
                $galleries = array_merge($galleries, $rows);
            }
        }

        $this->setGalleriesUrls($galleries);
        return $galleries;
    }

    private function setGalleriesUrls(array &$galleries): void
    {
        foreach ($galleries as $gallery) {
            $params = ['slug' => $gallery->getSlug()];
            $url = $this
                ->getUrlGenerator()
                ->generate(GalleryController::ROUTE_NAME . 'show', $params);

            $gallery->setUrl($url);
        }
    }

    private function save(Gallery $gallery = null, array $arguments = []): Response
    {
        $formParams = $this->buildFormParams($gallery);

        $eventName = $gallery ? Events::GALLERY_UPDATED : Events::GALLERY_CREATED;
        $gallery = $gallery ?: new Gallery();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $gallery,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: GalleryType::class,
            routeName: GalleryController::ROUTE_NAME,
            templatesPath: GalleryController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);      
    }

    private function buildFormParams(?Gallery $gallery): array
    {
        $params = $gallery ? ['slug' => $gallery->getSlug()] : [];
        return [
            'link' => ['route' => GalleryController::ROUTE_NAME . 'show', 'params' => $params],
            'item' => $gallery
        ];
    }
}