<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\AuthBundle\Entity\Group;
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\MediaBundle\Utils\MediaConfig;
use Symfony\Component\String\Slugger\AsciiSlugger;

class GalleryHandler extends MediaConfig
{
    private ?Gallery $clonedGallery = null;
    private ?Gallery $gallery = null;
    private bool $delete = false;

    public function setDelete(): static
    {
        $this->delete = true;
        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(Gallery $gallery): static
    {
        $this->gallery = $gallery;
        return $this;
    }

    public function getClonedGallery(): ?Gallery
    {
        return $this->clonedGallery;
    }

    public function setClonedGallery(Gallery $gallery): static
    {
        $this->clonedGallery = $gallery;
        return $this;
    }

    public function handle(): static
    {
        if ($this->delete) {
            $this->em->getRepository(Gallery::class)->setToPrevParent($this->gallery);
            $this->gallery->getMediafiles()->clear();
            $this->em->remove($this->gallery);
            $this->handleFolder();
        } else {
            $this->handleGalery()->handleFolder()->synchronizeChildGroups($this->gallery);
            $this->synchronizeParentGroups($this->gallery);
        }

        $this->em->flush();
        return $this;
    }

    private function handleGalery(): static
    {
        $slug = (new AsciiSlugger())->slug($this->gallery->getTitle());
        $this->gallery->setSlug($slug);
        $this->setGroupForWebgallery();
        $this->em->persist($this->gallery);
        return $this;
    }

    private function setGroupForWebgallery(): void
    {
        // Web galleries should only ever be assigned to the author group
        $parentGallery = $this->gallery->getParent();
        if ($parentGallery && $parentGallery->isWeb()) {
            $this->gallery->getGroups()->clear();
            $authorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => BlogData::AUTHOR_GROUP]);
            $this->gallery->addGroup($authorGroup)->setWeb(true);
        }
    }

    private function handleFolder(): static
    {
        $galleryDir = $this->gallery->isWeb() ? $this->getPublicGalleriesDir() : $this->getGalleriesDir();
        $galleryMediaDir = $galleryDir . $this->gallery->getMediaDir();

        if ($this->delete) {
            $this->filesystem->remove($galleryMediaDir);
            return $this;
        }

        if (!$this->clonedGallery && !$this->filesystem->exists($galleryMediaDir)) {
            $this->filesystem->mkdir($galleryMediaDir, 0755);
        } elseif ($this->clonedGallery) {
            $this->handleGalleryMediaDir($galleryDir);
        }

        return $this;
    }

    private function handleGalleryMediaDir(string $galleryDir): void
    {
        $clonedGallerySlug = $this->clonedGallery->getSlug();
        $gallerySlug = $this->gallery->getSlug();

        if ($gallerySlug != $clonedGallerySlug) {
            $originDir = $galleryDir . $this->clonedGallery->getMediaDir();
            $targetDir = $galleryDir . $this->gallery->getMediaDir();
            $this->filesystem->rename($originDir, $targetDir, true);
        }
    }

    private function synchronizeChildGroups(Gallery $gallery): void
    {
        $groups = $gallery->getGroups();
        if ($groups->count()) {
            $rows = $this->em->getRepository(Gallery::class)->findChilds($gallery);
            foreach ($rows as $row) {
                $row->getGroups()->clear();
                foreach ($groups as $group) $row->addGroup($group);
                $this->em->persist($row);
                $this->synchronizeChildGroups($row);
            }
        }
    }

    private function synchronizeParentGroups(Gallery $gallery): void
    {
        $parentGallery = $gallery->getParent();
        if ($parentGallery !== null) {

            $groups = $gallery->getGroups();
            $parentGroups = $parentGallery->getGroups();

            if ($groups->count() && $parentGroups->count()) {
                foreach ($groups as $group) {
                    if (!$parentGroups->contains($group)) $parentGallery->addGroup($group);
                }
                $this->em->persist($parentGallery);
                $this->synchronizeParentGroups($parentGallery);
            } elseif (!$groups->count() && $parentGroups->count()) {
                foreach ($parentGroups as $group) $gallery->addGroup($group);
            }
        }
    }
}