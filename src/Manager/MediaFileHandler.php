<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Manager;

use Allmega\BlogBundle\Utils\File;
use Allmega\AuthBundle\Entity\Group;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\MediaBundle\Utils\MediaConfig;
use Allmega\MediaBundle\Entity\{MediaFile, Mediatype};
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Finder\Finder;

class MediaFileHandler extends MediaConfig
{
    private string $dir = '';
    private ?MediaFile $mediaFile = null;
    private ?MediaFile $clonedMediaFile = null;
    private ?UploadedFile $uploadedFile = null;
    private string $mediatypeShortname = '';
    private array $mediatypes = [];
    private bool $delete = false;

    public function setDelete(): static
    {
        $this->delete = true;
        return $this;
    }

    public function getMediaFile(): ?MediaFile
    {
        return $this->mediaFile;
    }

    public function setMediaFile(MediaFile $mediaFile): static
    {
        $this->mediaFile = $mediaFile;
        return $this;
    }

    public function setClonedMediaFile(MediaFile $mediaFile): static
    {
        $this->clonedMediaFile = clone $mediaFile;
        return $this;
    }

    public function setUploadedFile(UploadedFile $file): static
    {
        $this->uploadedFile = $file;
        return $this;
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function setDir(string $path = ''): static
    {
        if ($path && strpos($path, '_')) $this->dir = $this->getCMSDir() . str_replace('_', DIRECTORY_SEPARATOR, $path);
        else $this->dir = $this->getMediaFilePath($this->mediaFile, false) . $this->mediaFile->getMediaDir();

        return $this;
    }

    public function getMediaFilePath(MediaFile $mediaFile, bool $flag = true): string
    {
        $path = $mediaFile->isWeb() ? $this->getPublicGalleriesDir() : $this->getGalleriesDir();
        return $flag ? $path . $mediaFile->getRelativePath() : $path;
    }

    public function handle(): static
    {
        if ($this->delete) {
            $fileName = $this->mediaFile->isWeb() ? $this->mediaFile->getFilename() : $this->mediaFile->getId();
            $this->remove($fileName);
            $this->em->remove($this->mediaFile);
            $this->em->flush();
            return $this;
        }

        $this->handleMediafile()->handleFile();
        return $this;
    }

    private function handleMediafile(): static
    {
        $slug = (new AsciiSlugger('de'))->slug($this->mediaFile->getTitle());
        $this->mediaFile->setSlug($slug);

        // Media files for the web galleries should only ever be assigned to the "Author" group
        if ($this->mediaFile->isWeb()) {
            $this->mediaFile->getGroups()->clear();
            $authorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => BlogData::AUTHOR_GROUP]);
            $this->mediaFile->addGroup($authorGroup);
        } else {
            if ($this->mediaFile->getGroups()->count() < 1) {
                $groups = $this->mediaFile->getGallery()->getGroups();
                foreach ($groups as $group) $this->mediaFile->addGroup($group);
            }
        }

        $this->em->persist($this->mediaFile);
        $this->em->flush();

        if ($this->uploadedFile) {
            $mimeType = $this->uploadedFile->getMimeType();
            $size = $this->uploadedFile->getSize();
            $fileName = $this->move();

            $this->mediaFile->setFilename($fileName)->setFiletype($mimeType)->setFilesize($size);
        }

        $this->em->persist($this->mediaFile);
        return $this;
    }

    private function handleFile(): void
    {
        /**
         * When editing, a different gallery can be assigned to the media file
         * If a new file is uploaded in the process, then simply delete the old file
         * Otherwise move the existing file to the gallery folder
         */
        if ($this->clonedMediaFile) {
            $oldFilePath = $this->getMediaFilePath($this->clonedMediaFile);
            $newFilePath = $this->getMediaFilePath($this->mediaFile);

            if ($oldFilePath != $newFilePath) {
                if (!$this->filesystem->exists($this->dir))  $this->filesystem->mkdir($this->dir, 0755);
                if ($this->filesystem->exists($newFilePath)) $this->filesystem->remove($oldFilePath);
                if ($this->filesystem->exists($oldFilePath)) $this->filesystem->rename($oldFilePath, $newFilePath);
            }
        }
    }

    public function move(): string
    {
        $originalFilename = pathinfo($this->uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = (new AsciiSlugger('de'))->slug($originalFilename);
        $filename = $safeFilename . '.' . $this->uploadedFile->guessExtension();

        if (!$this->filesystem->exists($this->dir)) $this->filesystem->mkdir($this->dir, 0755);

        $tmpName = $this->mediaFile && !$this->mediaFile->isWeb() ? $this->mediaFile->getId() : $filename;
        $this->uploadedFile->move($this->dir, $tmpName);

        return $filename;
    }

    public function remove(string $fileName = ''): void
    {
        $path = $this->dir;
        if ($fileName) $path .= DIRECTORY_SEPARATOR . $fileName;
        if ($this->filesystem->exists($path)) $this->filesystem->remove($path);
    }

    public function getMediatypes(string $shortname = ''): array
    {
        if (!$this->mediatypes || $shortname != $this->mediatypeShortname) {
            $this->mediatypeShortname = $shortname;
            $this->setMediatypes();
        }

        $mediatypes = [];
        $files = $this->findFiles();

        foreach ($this->mediatypes as $mediatype) {
            if ($mediatype->getFiles()) $mediatype->setFiles();

            $mimetypes = $mediatype->getMimetypesArray();
            foreach ($files as $file) {
                if (in_array($file->getMimetype(), $mimetypes)) $mediatype->addFile($file);
            }

            if ($mediatype->getFiles()) $mediatypes[] = $mediatype;
        }
        return $mediatypes;
    }

    public function setMediatypes(): static
    {
        $criteria = $this->mediatypeShortname ? ['shortname' => $this->mediatypeShortname] : [];
        $this->mediatypes = $this->em->getRepository(Mediatype::class)->findBy($criteria, ['name' => 'ASC']);
        return $this;
    }

    public function findFiles(): array
    {
        $files = [];
        if ($this->filesystem->exists($this->dir)) {
            $finder = new Finder();
            $finder->depth('== 0')->files()->in($this->dir);
            if ($finder->hasResults()) {
                foreach ($finder as $file) $files[] = new File($file);
            }
        }
        return $files;
    }

    public function findFileById(string $id): File|null
    {
        if ($this->filesystem->exists($this->dir)) {
            $finder = new Finder();
            $finder->depth('== 0')->name($id . '.*')->files()->in($this->dir);
            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    return new File($file);
                }
            }
        }
        return null;
    }
}