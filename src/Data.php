<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\MediaBundle\Controller\{GalleryController, MediatypeController};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\MediaBundle\Entity\{Gallery, Mediatype};
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Entity\Group;

class Data extends PackageData
{
	public const DOMAIN = 'AllmegaMediaBundle';
	public const PACKAGE = 'media';

	public const DOWNLOADS_GALLERY = 'Downloads';
	public const MEDIA_GALLERY = 'Web';

	public const GROUP_TYPE_MEDIA = 'media.main';

	public const MANAGER_GROUP = 'media.manager';
    public const AUTHOR_GROUP = 'media.author';
    public const USER_GROUP = 'media.user';

	public const MANAGER_ROLE = 'ROLE_MEDIA_MANAGER';
	public const AUTHOR_ROLE = 'ROLE_MEDIA_AUTHOR';
	public const USER_ROLE = 'ROLE_MEDIA_USER';

	protected function setRegisterData(): void
	{
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
        ];
	}

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'menupoints' => $this->getMenuPoints(),
            'mediatypes' => $this->getMediaTypes(),
            'grouptypes' => $this->getGroupTypes(),
            'galleries' => $this->getGalleries(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
        ];
    }

	/**
	 * @return array<int,ControllerEntry>
	 */
	protected function getControllerEntries(): array
	{
		return [
			new ControllerEntry(
                ControllerEntriesMap::PROFILE_MENU,
                self::PACKAGE,
                'gallery',
                GalleryController::class,
                'getMenuIcon'),
		];
	}

    /**
     * @return array<int,MenupointEntry>
     */
	protected function getMenuPoints(): array
	{
		$main = 'media.main';
		$menuPoints = [
			new MenupointEntry('media.mediatypes', MediatypeController::ROUTE_NAME . 'index', [], $this->routeType, 2, [self::MANAGER_GROUP], $main),
			new MenupointEntry('media.galleries', GalleryController::ROUTE_NAME . 'show', [], $this->routeType, 1, [self::USER_GROUP], $main),
			new MenupointEntry($main, '', [], $this->menuType, 4, [], BlogData::MENUPOINT_BLOG_MANAGE),
		];
		return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
	}

    /**
     * @return array<int,Gallery>
     */
	protected function getGalleries(): array
	{
		$authors = [Group::build(shortname: BlogData::AUTHOR_GROUP)];
		$galleries = [
            Gallery::build(self::MEDIA_GALLERY, self::MEDIA_GALLERY, true, true, $authors),
            Gallery::build(self::DOWNLOADS_GALLERY, self::DOWNLOADS_GALLERY, false, true),
        ];
		return [self::PACKAGE => $galleries];
	}

    /**
     * @return array<int,MediaType>
     */
	protected function getMediaTypes(): array
	{
		$archives = <<<TEXT
application/zip
application/gzip
application/x-bzip
application/x-bzip2
application/x-tar
TEXT;

		$msoffice = <<<TEXT
text/plain
application/pdf
application/msword
application/msexcel
application/vnd.ms-excel
application/vnd.ms-powerpoint
application/vnd.openxmlformats-officedocument.wordprocessingml.document
application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
application/vnd.openxmlformats-officedocument.presentationml.slideshow
TEXT;

		$types = [
            Mediatype::build('media.pictures', "image/jpeg\nimage/gif\nimage/png", true, true),
            Mediatype::build('media.videos', "video/x-msvideo\nvideo/mp4", true, true),
            Mediatype::build('media.documents', $msoffice, true, true),
            Mediatype::build('media.archives', $archives, true, true),
        ];
		return [self::PACKAGE => $types];
	}

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_MEDIA => [
                self::MANAGER_GROUP => [
                    self::MANAGER_ROLE => 'media.manager',
                    self::AUTHOR_ROLE => 'media.author',
                    self::USER_ROLE => 'media.user',
                ],
                self::AUTHOR_GROUP => [
                    self::AUTHOR_ROLE => 'media.author',
                    self::USER_ROLE => 'media.user',
                ],
                self::USER_GROUP => [
                    self::USER_ROLE => 'media.user',
                ],
            ],
        ];
    }
}