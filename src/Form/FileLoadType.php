<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Form;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Entity\Mediatype;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\{All, File};
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class FileLoadType extends AbstractType
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly EntityManagerInterface $em,
        private readonly string $maxFileSize) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $helpMessage = $this->translator->trans('mediafile.help.max_size', [], Data::DOMAIN);
        $params = [
            'label' => false,
            'mapped' => false,
            'required' => false,
            'help' => $helpMessage,
            'help_translation_parameters' => ['%size%' => $this->maxFileSize]
        ];

        foreach ($options['attr'] as $key => $val) $params[$key] = $val;
        $params['constraints'] = isset($params['multiple']) && $params['multiple'] ? [$this->getAll()] : [$this->getFile()];

        $builder->add('file', FileType::class, $params);
    }

    private function getFile(): File
    {
        $mimeTypesMessage = $this->translator->trans('mediafile.valid_mimetype', [], Data::DOMAIN);
        return new File(
            maxSize: $this->maxFileSize,
            mimeTypes: $this->getMimetypes(),
            mimeTypesMessage: $mimeTypesMessage,
        );
    }

    private function getAll(): All
    {
        return new All($this->getFile());
    }

    private function getMimetypes(): array
    {
        $mimetypes = [];
        $mediatypes = $this->em->getRepository(Mediatype::class)->findAll();

        foreach ($mediatypes as $mediatype) {
            $mimetypes = array_merge($mimetypes, $mediatype->getMimetypesArray());
        }
        return $mimetypes;
    }
}