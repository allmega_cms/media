<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Form;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\MediaBundle\Model\MediaTypeTrait;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\ORM\EntityRepository;

class GalleryType extends AbstractType
{
    use MediaTypeTrait;

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly Security $security) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $isGranted = $this->security->isGranted('media-gallery-web');
        $gallery = $builder->getData();

        $builder
            ->add('parent', EntityType::class, [
                'label' => 'gallery.label.parent',
                'help'  => 'gallery.help.parent',
                'required' => false,
                'class' => Gallery::class,
                'choice_label'  => 'title',
                'query_builder' => function (EntityRepository $er) use ($gallery, $isGranted) {
                    $query = $er->createQueryBuilder('g')->orderBy('g.title', 'ASC');
                    if ($isGranted && $gallery->isWeb()) $query->andWhere('g.web = 1');
                    
                    if ($gallery->getId()) {
                        // The gallery must not be assigned to itself
                        $query->andWhere('g.id != :id')->setParameter('id', $gallery->getId());
                        // The gallery must not be assigned to its child galleries
                        $query->andWhere('g.parent != :gid OR g.parent IS NULL')->setParameter('gid', $gallery->getId());
                        if (!$gallery->isWeb()) $query->andWhere('g.web = 0');
                    }
                    return $query;
                },
            ]);

        $this->buildBaseForm($builder, $gallery);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}