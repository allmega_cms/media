<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\MediaBundle\Form;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Entity\Mediatype;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'mediatype.label.name'],
                'label' => 'mediatype.label.name'
            ])
            ->add('shortname', null, [
                'attr' => ['placeholder' => 'mediatype.label.shortname'],
                'help' => 'mediatype.help.shortname',
                'label' => 'mediatype.label.shortname'
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'label.description'],
                'label' => 'label.description'
            ])
            ->add('mimetypes', TextareaType::class, [
                'help' => 'mediatype.help.mimetypes',
                'label' => 'mediatype.label.mimetypes',
                'attr' => ['rows' => 7, 'placeholder' => 'mediatype.label.mimetypes']
            ])
            ->add('active', null, [
                'label' => 'mediatype.label.active',
                'help' => 'mediatype.help.active'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Mediatype::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}