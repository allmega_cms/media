<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Form;

use Allmega\MediaBundle\Data;
use Allmega\MediaBundle\Model\MediaTypeTrait;
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\ORM\EntityRepository;

class MediaFileType extends AbstractType
{
    use MediaTypeTrait;

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly Security $security) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $isGranted = $this->security->isGranted('media-gallery-web');
        $gallery = $builder->getData()->getGallery();

        $builder
            ->add('gallery', EntityType::class, [
                'label' => 'mediafile.label.gallery',
                'help'  => 'mediafile.help.gallery',
                'class' => Gallery::class,
                'choice_label'  => 'title',
                'query_builder' => function (EntityRepository $er) use ($gallery, $isGranted) {
                    $web = (int) $isGranted && $gallery->isWeb();
                    return $er->createQueryBuilder('g')
                        ->andWhere('g.web = :web')
                        ->setParameter('web', $web)
                        ->orderBy('g.title', 'ASC');
                }
            ]);

        $this->buildBaseForm($builder, $gallery, 'mediafile');
    }

    public function getParent(): ?string
    {
        return 'Allmega\MediaBundle\Form\FileLoadType';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MediaFile::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}