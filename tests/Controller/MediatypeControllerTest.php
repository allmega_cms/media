<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Tests\Controller;

use Allmega\MediaBundle\Controller\MediatypeController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MediaBundle\Entity\Mediatype;
use Allmega\MediaBundle\Data;

class MediatypeControllerTest extends AllmegaWebTest
{
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::MANAGER_ROLE);
    }

    public function testAdd(): void
    {
        $this->runTests($this->add, Data::MANAGER_ROLE);
    }

    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::MANAGER_ROLE, true);
    }

    public function testChangeState(): void
    {
        $this->runModifyTests(redirectName: $this->index, role: Data::MANAGER_ROLE);
    }

    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::MANAGER_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = Mediatype::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return MediatypeController::ROUTE_NAME . $name;
    }
}