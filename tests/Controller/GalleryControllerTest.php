<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Tests\Controller;

use Allmega\MediaBundle\Controller\GalleryController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MediaBundle\Entity\Gallery;
use Allmega\MediaBundle\Data;

class GalleryControllerTest extends AllmegaWebTest
{
    public function testMenuIcon(): void
    {
        $this->runTests($this->icon, Data::USER_ROLE, false, false);
    }

    public function testBuildMenu(): void
    {
        $this->runTests('menu', Data::USER_ROLE, false, false);
    }

    public function testShow(): void
    {
        $this->runTests(
            fnRouteParams: 'getSlugAsRouteParams',
            routeName: $this->show,
            role: Data::USER_ROLE,
            testRedirect: false,
            create: true);
    }

    public function testAdd(): void
    {
        $this->runTests($this->add, Data::USER_ROLE);
    }

    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::USER_ROLE, true);
    }

    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::USER_ROLE, delete: true, redirectName: $this->show);
    }

    protected function create(): void
    {
        $user = $this->findUserByRole(Data::USER_ROLE);
        $entity = Gallery::build(user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return GalleryController::ROUTE_NAME . $name;
    }
}