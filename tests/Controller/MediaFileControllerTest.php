<?php

/**
 * This file is part of the Allmega Media Bundle package.
 *
 * @copyright Allmega 
 * @package   Media Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MediaBundle\Tests\Controller;

use Allmega\MediaBundle\Controller\{GalleryController, MediaFileController};
use Allmega\MediaBundle\Entity\{Gallery, MediaFile};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MediaBundle\Data;

class MediaFileControllerTest extends AllmegaWebTest
{
    public function testLoad(): void
    {
        $this->assertEquals(true, true);
    }

    public function testAdd(): void
    {
        $this->runTests(
            routeParams: ['slug' => 'Downloads'],
            routeName: $this->add,
            role: Data::USER_ROLE);
    }

    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::USER_ROLE, true);
    }

    public function testDelete(): void
    {
        $routeRedirect = GalleryController::ROUTE_NAME . $this->show;
        $this->runModifyTests(
            redirectRoute: $routeRedirect,
            role: Data::USER_ROLE,
            delete: true);
    }

    protected function create(): void
    {
        $gallery = $this->em->getRepository(Gallery::class)->findOneBy(['slug' => 'Downloads']);
        $user = $this->findUserByRole(Data::USER_ROLE);

        $entity = MediaFile::build(gallery: $gallery, user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return MediaFileController::ROUTE_NAME . $name;
    }
}